package ee.bcs.valiit.bank.service;

import ee.bcs.valiit.bank.data.Account;
import ee.bcs.valiit.bank.data.AccountRepository;
import ee.bcs.valiit.bank.data.Customer;
import ee.bcs.valiit.bank.data.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private AccountRepository accountRepository;

    public List<Customer> list() {
        return customerRepository.findAll();
    }

    public Customer get(Long id) {
        return customerRepository.getOne(id);
    }

    @Transactional
    //kõik, mida siit edasi kutsutakse on translatsioonilised ehk keritakse tagasi; delete on ALATI transactional
    public void delete(Customer customer) {
        customerRepository.delete(customer);
    }

    @Transactional
    public void save(Customer customer) {
        Customer dbCustomer = customerRepository.save(customer);
        if (customer.getId() == 0) {
            Account account = new Account();
            boolean notUnique = true;
            long number = 0L;
            while (notUnique) {
                number = ThreadLocalRandom.current().nextLong(100000000);
                Account tmp = accountRepository.findByAccountNumber(number);
                if (tmp == null) {
                    notUnique = false;
                }
            }
            account.setAccountNumber(number);
            account.setBalance(new BigDecimal(0));
            account.setCustomerId(dbCustomer.getId());
            account.setActive(true);
            accountRepository.save(account);
        }

    }

}
