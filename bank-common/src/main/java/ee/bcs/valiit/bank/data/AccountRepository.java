package ee.bcs.valiit.bank.data;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
public interface AccountRepository extends JpaRepository<Account, Long> {

    // List<Account> findByCustomerId(Long id);

    Account findByAccountNumber(Long accountNumber);
}
